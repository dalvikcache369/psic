package com.psic.constants;

import lombok.Getter;

public class Constants {
	
	@Getter
	public enum expertise{
		Physiotherapy, Osteotherapy, Rehabilitation,Accupunture;
	}
	
	@Getter
	public enum bookingStatus{
		BOOKED("Booked"),CANCELLED("Cancelled"),ATTENDED("Attended"),MISSED("Missed");
		
		public String name;

		bookingStatus(String name) {
			this.name= name;
		}
	}

}
