package com.psic.request.models;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class AppointmentRequest {
	private Long patientId;
	private Long physicianId;
	private String notes;
	private String expertise;
	private String room;
	private Date date;

	public AppointmentRequest(Long patientId, Long physicianId, String expertise, String room, Date date) {
		this.patientId = patientId;
		this.physicianId = physicianId;
		this.expertise = expertise;
		this.room = room;
		this.date = date;
	}

	public AppointmentRequest(Long physicianId, String notes, String expertise, String room, Date date) {
		this.physicianId = physicianId;
		this.notes = notes;
		this.expertise = expertise;
		this.room = room;
		this.date = date;
	}

}
