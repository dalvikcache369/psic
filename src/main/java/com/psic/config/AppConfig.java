package com.psic.config;

import java.sql.Date;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.psic.controller.PatientController;
import com.psic.controller.PsicController;
import com.psic.controller.TreatmentController;
import com.psic.models.Patient;
import com.psic.models.Physician;
import com.psic.request.models.AppointmentRequest;

@Order(value = 2)
@Component
public class AppConfig implements ApplicationRunner {

	@Autowired
	private PatientController patientController;
	@Autowired
	private PsicController psicController;
	@Autowired
	private TreatmentController treatController;

	// @PostConstruct
	@Override
	public void run(ApplicationArguments args) throws Exception {
		System.out.println("Running Spring Boot Application");
		do {
			Scanner myScanner = new Scanner(System.in);
			System.out.println("\n--------------------------------------------------------------\n");
			System.out.println("1. Register A Patient.");
			System.out.println("2. Book Treatment Appointment.");
			System.out.println("3. Cancel Booking.");
			System.out.println("4. Attend A Treatment Appointment.");
			System.out.println("5. Book A Visitor Appointment");
			System.out.println("6. Fetch report 1.");
			System.out.println("7. Fetch Report 2");
			System.out.println("\n--------------------------------------------------------------\n");

			int choice = myScanner.nextInt();

			switch (choice) {
			case 1:
				System.out.println("----------------------Patient Registeration.--------------------\n");
				System.out.println("Enter Patient Name : ");
				String name = myScanner.next();
				System.out.println("Enter Address : ");
				String address = myScanner.next();
				System.out.println("Enter Telephone Number : ");
				String number = myScanner.next();
				ResponseEntity<Patient> patient = patientController.addPatient(new Patient(name, address, number));
				if (null != patient.getBody() && patient.getStatusCode().equals(HttpStatus.OK)) {
					System.out.println("Patient registered Successfully with id : " + patient.getBody().getId());
				} else {
					System.out.println(patient.getBody() + " : " + patient.getStatusCode());
				}
				break;
			case 2:
				String expertise = "";
				System.out.println("-----------------Patient Appointment Booking.-------------------\n");
				System.out.println("Enter Patient Id : ");
				Long patientId = myScanner.nextLong();
				System.out.print("Enter Your option : ");
				System.out.println("\n--------------------------------------------------------------\n");
				System.out.println("1. Search Physician By Expertise.\n");
				System.out.println("2. Search Physician By Name.\n");
				System.out.println("3. Continue without search.");
				System.out.println("\n--------------------------------------------------------------\n");
				Integer option = myScanner.nextInt();
				switch (option) {
				case 1:
					System.out.println("Enter Expertise : ");
					expertise = myScanner.next();
					ResponseEntity<List<Physician>> physicians = psicController.getAllPhysician(expertise);
					if (null != physicians.getBody() && physicians.getStatusCode().equals(HttpStatus.OK)) {
						System.out.println("All Availble Physicians are : ");
						for (Physician ph : physicians.getBody()) {
							System.out.println("Id : " + ph.getId() + " Name : " + ph.getName());
						}
					} else {
						System.out.println("Physician Not Available for this expertise");
					}
					break;
				case 2:
					System.out.println("Enter Name : ");
					String phyname = myScanner.next();
					ResponseEntity<List<Physician>> physiciansName = psicController.getPhysicianByName(phyname);
					if (null != physiciansName.getBody() && physiciansName.getStatusCode().equals(HttpStatus.OK)) {
						System.out.println("All Availble Physicians are : ");
						for (Physician ph : physiciansName.getBody()) {
							System.out.println("Id : " + ph.getId() + " Name : " + ph.getName());
						}
					} else {
						System.out.println("Physician Not Available by this name");
					}
					break;
				case 3:
					break;
				default:
					break;
				}
				System.out.println("Enter Physician Id : ");
				Long physicianId = myScanner.nextLong();
				if (null == expertise || expertise.isEmpty()) {
					ResponseEntity<List<String>> expertises = psicController.getAllExpertise();
					if (null != expertises.getBody() && expertises.getStatusCode().equals(HttpStatus.OK)) {
						System.out.println("All Availble expertise are : " + expertises.getBody());
					}
					System.out.println("Enter Expertise : ");
					expertise = myScanner.next();
				}
				System.out.println("Enter Room : ");
				String room = myScanner.next();
				System.out.println("Enter Date in yy-mm-d format : ");
				Date date = Date.valueOf(myScanner.next());
				ResponseEntity<String> appointment = treatController
						.bookAppointment(new AppointmentRequest(patientId, physicianId, expertise, room, date));
				System.out.println(appointment.getBody());
				break;
			case 3:
				System.out.println("----------------------Modify Appointment.-----------------------\n");
				System.out.println("Enter Appointment Id to Cancel ");
				ResponseEntity<String> modify = treatController.cancleAppointment(myScanner.nextLong());
				System.out.println(modify.getBody());
				break;
			case 4:
				System.out.println("\n--------------------------------------------------------------\n");
				System.out.println("Attend Appointment.");
				System.out.println("\n--------------------------------------------------------------\n");
				System.out.println("Enter Appointment Id to Attend ");
				ResponseEntity<String> attend = treatController.attendAppointment(myScanner.nextLong());
				System.out.println(attend.getBody());
				break;
			case 5:
				String exp = "";
				System.out.println("-----------------Visitor Appointment Booking.--------------------\n");
				System.out.print("Enter Your option : ");
				System.out.println("\n--------------------------------------------------------------\n");
				System.out.println("1. Search Physician By Expertise.\n");
				System.out.println("2. Search Physician By Name.\n");
				System.out.println("3. Continue without search.");
				System.out.println("\n--------------------------------------------------------------\n");
				Integer options = myScanner.nextInt();
				switch (options) {
				case 1:
					System.out.println("Enter Expertise : ");
					exp = myScanner.next();
					ResponseEntity<List<Physician>> physicians = psicController.getAllPhysician(exp);
					if (null != physicians.getBody() && physicians.getStatusCode().equals(HttpStatus.OK)) {
						System.out.println("All Availble Physicians are : ");
						for (Physician ph : physicians.getBody()) {
							System.out.println("Id : " + ph.getId() + " Name : " + ph.getName());
						}
					} else {
						System.out.println("Physician Not Available for this expertise");
					}
					break;
				case 2:
					System.out.println("Enter Name : ");
					String phyname = myScanner.next();
					ResponseEntity<List<Physician>> physiciansName = psicController.getPhysicianByName(phyname);
					if (null != physiciansName.getBody() && physiciansName.getStatusCode().equals(HttpStatus.OK)) {
						System.out.println("All Availble Physicians are : ");
						for (Physician ph : physiciansName.getBody()) {
							System.out.println("Id : " + ph.getId() + " Name : " + ph.getName());
						}
					} else {
						System.out.println("Physician Not Available by this name");
					}
					break;
				case 3:
					break;
				default:
					break;
				}
				System.out.println("Enter Physician Id : ");
				Long physicId = myScanner.nextLong();
				ResponseEntity<List<String>> expertiseList = psicController.getAllExpertise();
				if (null != expertiseList.getBody() && expertiseList.getStatusCode().equals(HttpStatus.OK)) {
					System.out.println("All Availble expertise are : " + expertiseList.getBody());
				}
				System.out.println("Enter Visitor Name : ");
				String visitorName = myScanner.next();
				System.out.println("Enter Expertise : ");
				String expert = myScanner.next();
				System.out.println("Enter Room : ");
				String rooms = myScanner.next();
				System.out.println("Enter Date in yy-mm-d format : ");
				Date dates = Date.valueOf(myScanner.next());
				ResponseEntity<String> appointments = treatController
						.bookAppointment(new AppointmentRequest(physicId, visitorName, expert, rooms, dates));
				System.out.println(appointments.getBody());
				break;
			case 6:
				System.out.println("-----------------------------Report 1.--------------------------\n");
				treatController.fetchReport1();
				break;
			case 7:
				System.out.println("-----------------------------Report 2.--------------------------\n");
				treatController.fetchReport2();
				break;
			default:
				// System.exit(0);
				break;
			}
			myScanner.close();
		} while (true);
	}
}
