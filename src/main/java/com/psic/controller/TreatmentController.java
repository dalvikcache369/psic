package com.psic.controller;

import java.sql.Date;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.psic.constants.Constants.bookingStatus;
import com.psic.models.Patient;
import com.psic.models.Physician;
import com.psic.models.TimeSlot;
import com.psic.models.Treatment;
import com.psic.repository.PatientRepository;
import com.psic.repository.PhysicianRepository;
import com.psic.repository.TimeSlotRepository;
import com.psic.repository.TreatmentRepository;
import com.psic.request.models.AppointmentRequest;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
public class TreatmentController {
	@Autowired
	private PhysicianRepository phyRepo;
	@Autowired
	private PatientRepository patientRepo;
	@Autowired
	private TreatmentRepository treatRepo;
	@Autowired
	private TimeSlotRepository timeSlotRepo;

	@PostMapping("bookAppointment")
	public ResponseEntity<String> bookAppointment(@RequestBody AppointmentRequest appointment) {
		try {
			if (null != appointment) {
				Date date = new Date(System.currentTimeMillis());
				if (appointment.getDate().toLocalDate().isBefore(date.toLocalDate())) {
					return new ResponseEntity<>("Past Date Booking is Not Allowed.", HttpStatus.OK);
				}
				if (null != appointment.getPhysicianId() && null != appointment.getDate()) {
					TimeSlot timeSlot = timeSlotRepo.getSlotByDate(appointment.getPhysicianId(), appointment.getDate());
					if (null != timeSlot) {
						if (timeSlot.getSlot1() || timeSlot.getSlot2() || timeSlot.getSlot3() || timeSlot.getSlot4()) {
							Integer availableSlot = timeSlot.getSlot1() ? 0
									: timeSlot.getSlot2() ? 1 : timeSlot.getSlot3() ? 2 : 3;
							Optional<Physician> physician = phyRepo.findById(appointment.getPhysicianId());
							if (physician.isPresent()) {
								LocalTime localtime = physician.get().getStartTime().toLocalTime();
								localtime = localtime.plusMinutes(availableSlot * 30);
								String startTime = localtime.toString();
								String endTime = localtime.plusMinutes(30).toString();
								List<Treatment> treatments = treatRepo
										.findByPatientIdAndDate(appointment.getPatientId(), appointment.getDate());
								Treatment treatment = treatments.stream()
										.filter(e -> e.getSlot().equalsIgnoreCase(startTime + " : " + endTime) && e.getBookingStatus().equalsIgnoreCase(bookingStatus.BOOKED.name))
										.findAny().orElse(null);
								if (null != treatment) {
									return new ResponseEntity<>("Patient Already signed up for another appointment.",
											HttpStatus.OK);
								} else {
									if (null != appointment.getPatientId()) {
										Optional<Patient> patient = patientRepo.findById(appointment.getPatientId());
										if (patient.isPresent()) {
											Treatment t = new Treatment(appointment.getPatientId(),
													patient.get().getName(), appointment.getExpertise(),
													appointment.getRoom(), appointment.getDate(),
													startTime + " : " + endTime, appointment.getPhysicianId(),
													bookingStatus.BOOKED.name, availableSlot);
											treatRepo.save(t);
											updateTimeSlot(availableSlot, timeSlot);
											return new ResponseEntity<>("Appointment Booked for Slot - " + startTime
													+ " : " + endTime + "\n Appointment Id is - " + t.getId(),
													HttpStatus.OK);
										}
									} else if (null != appointment.getNotes() && !appointment.getNotes().isEmpty()) {
										Treatment t = new Treatment(appointment.getNotes(), appointment.getExpertise(),
												appointment.getRoom(), appointment.getDate(),
												startTime + " : " + endTime, appointment.getPhysicianId(),
												bookingStatus.BOOKED.name, availableSlot);
										treatRepo.save(t);
										updateTimeSlot(availableSlot, timeSlot);
										return new ResponseEntity<>("Appointment Booked for Slot : " + startTime + " : "
												+ endTime + "\n Appointment Id is - " + t.getId(), HttpStatus.OK);
									}
								}
							} else {
								return new ResponseEntity<>("Physician Not Found", HttpStatus.OK);
							}
						} else {
							return new ResponseEntity<>("Slot Not Available for this date", HttpStatus.OK);
						}
					} else {
						return new ResponseEntity<>("Time Slot Not found for this Date", HttpStatus.OK);
					}
				} else {
					return new ResponseEntity<>("Date or Physician Id is Empty", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<>("Request Data Empty", HttpStatus.OK);
			}
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private void updateTimeSlot(Integer availableSlot, TimeSlot timeSlot) {
		try {
			if (availableSlot == 0) {
				timeSlot.setSlot1(Boolean.FALSE);
			} else if (availableSlot == 1) {
				timeSlot.setSlot2(Boolean.FALSE);
			} else if (availableSlot == 2) {
				timeSlot.setSlot3(Boolean.FALSE);
			} else {
				timeSlot.setSlot4(Boolean.FALSE);
			}
			timeSlotRepo.save(timeSlot);
		} catch (Exception e) {

		}

	}

	@PostMapping("cancleAppointment/{appointmentId}")
	public ResponseEntity<String> cancleAppointment(@PathVariable("appointmentId") Long appointmentId) {
		try {
			if (null != appointmentId) {
				Optional<Treatment> treatment = treatRepo.findById(appointmentId);
				if (treatment.isPresent()) {
					Treatment book = treatment.get();
					Date date = new Date(System.currentTimeMillis());
					if (book.getDate().toLocalDate().isBefore(date.toLocalDate())) {
						return new ResponseEntity<>("Past Date Booking is Not Allowed.", HttpStatus.OK);
					} else {
						book.setBookingStatus(bookingStatus.CANCELLED.getName());
						treatRepo.saveAndFlush(book);
						TimeSlot timeSlot = timeSlotRepo.getSlotByDate(book.getPhysicianId(), book.getDate());
						if (null != timeSlot) {
							if (book.getAvailableSlot() == 0) {
								timeSlot.setSlot1(Boolean.TRUE);
							} else if (book.getAvailableSlot() == 1) {
								timeSlot.setSlot2(Boolean.TRUE);
							} else if (book.getAvailableSlot() == 2) {
								timeSlot.setSlot3(Boolean.TRUE);
							} else {
								timeSlot.setSlot4(Boolean.TRUE);
							}
							timeSlotRepo.save(timeSlot);
						}
						return new ResponseEntity<>("Booking Cancelled.", HttpStatus.NO_CONTENT);
					}

				} else {
					return new ResponseEntity<>("Appointment Not Found for appointment Id : " + appointmentId,
							HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<>("Appointment Id is null", HttpStatus.OK);
			}

		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("fetchReport1")
	public void fetchReport1() {
		try {
			List<Treatment> treatment = treatRepo.findAll();
			if (null != treatment && !treatment.isEmpty()) {
				System.out.println("All Appointment Table");
				final Object[][] appointmnetTable = new String[4][];
				appointmnetTable[0] = new String[] { "Physician Name", "Treatment Name", "Patient Name", "Time", "Room",
						"Visitor Name" };
				int j = 1;
				for (Treatment pat : treatment) {
					Optional<Physician> physic = phyRepo.findById(pat.getPhysicianId());
					appointmnetTable[j++] = new String[] { physic.get().getName(), pat.getExpertise(),
							pat.getPatientName(), pat.getSlot(), pat.getRoom(), pat.getVisitorName() };
				}
				for (final Object[] row : appointmnetTable) {
					if (null != row) {
						System.out.format("%15s%15s%15s%n", row);
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	@GetMapping("fetchReport2")
	public void fetchReport2() {
		try {
			List<Treatment> treatment = treatRepo.findAll();
			if (null != treatment && !treatment.isEmpty()) {
				List<Treatment> patient = treatment.stream()
						.filter(e -> null == e.getVisitorName() || e.getVisitorName().isEmpty())
						.collect(Collectors.toList());
				if (null != patient && !patient.isEmpty()) {
					System.out.println("Patient Appointment Table");
					final Object[][] patientTable = new String[4][];
					patientTable[0] = new String[] { "Physician Name", "Treatment Name", "Patient Name", "Time", "Room",
							"Booking Status" };
					int j = 1;
					for (Treatment pat : patient) {
						Optional<Physician> physic = phyRepo.findById(pat.getPhysicianId());
						patientTable[j++] = new String[] { physic.get().getName(), pat.getExpertise(),
								pat.getPatientName(), pat.getSlot(), pat.getRoom(), pat.getBookingStatus() };
					}
					for (final Object[] row : patientTable) {
						if (null != row) {
							System.out.format("%15s%15s%15s%n", row);
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@PostMapping("attendAppointment/{appointmentId}")
	public ResponseEntity<String> attendAppointment(@PathVariable("appointmentId") Long appointmentId) {
		try {
			if (null != appointmentId) {
				Optional<Treatment> treatment = treatRepo.findById(appointmentId);
				if (treatment.isPresent()) {
					Treatment book = treatment.get();
					Date date = new Date(System.currentTimeMillis());
					if (book.getDate().toLocalDate().isBefore(date.toLocalDate())) {
						return new ResponseEntity<>("Past Date Booking is Not Allowed.", HttpStatus.OK);
					} else {
						book.setBookingStatus(bookingStatus.ATTENDED.getName());
						treatRepo.saveAndFlush(book);
						TimeSlot timeSlot = timeSlotRepo.getSlotByDate(book.getPhysicianId(), book.getDate());
						if (null != timeSlot) {
							if (book.getAvailableSlot() == 0) {
								timeSlot.setSlot1(Boolean.TRUE);
							} else if (book.getAvailableSlot() == 1) {
								timeSlot.setSlot2(Boolean.TRUE);
							} else if (book.getAvailableSlot() == 2) {
								timeSlot.setSlot3(Boolean.TRUE);
							} else {
								timeSlot.setSlot4(Boolean.TRUE);
							}
							timeSlotRepo.save(timeSlot);
						}
						return new ResponseEntity<>("Appointment Attended.", HttpStatus.NO_CONTENT);
					}

				} else {
					return new ResponseEntity<>("Appointment Not Found for appointment Id : " + appointmentId,
							HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<>("Appointment Id is null", HttpStatus.OK);
			}

		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
