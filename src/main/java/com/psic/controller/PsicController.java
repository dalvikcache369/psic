package com.psic.controller;

import java.sql.Date;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.psic.constants.Constants.expertise;
import com.psic.models.Physician;
import com.psic.models.TimeSlot;
import com.psic.repository.PhysicianRepository;
import com.psic.repository.TimeSlotRepository;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
public class PsicController {
	@Autowired
	PhysicianRepository phyRepo;
	@Autowired
	TimeSlotRepository timeSlotRepo;
	
	@GetMapping("/expertise")
	public ResponseEntity<List<String>> getAllExpertise() {
		try {
			List<String> expertiseAll = new ArrayList<>();
			expertiseAll = Stream.of(expertise.values()).map(Enum::name).collect(Collectors.toList());
			if (expertiseAll.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(expertiseAll, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/physician/{expertises}")
	public ResponseEntity<List<Physician>> getAllPhysician(@PathVariable("expertises") String expertises) {
		try {
			List<Physician> physician = new ArrayList<Physician>();
			if (expertises != null && !expertises.isEmpty()) {
				phyRepo.findByExpertise(expertises).forEach(physician::add);
			}

			if (physician.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(physician, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/physicianByName/{name}")
	public ResponseEntity<List<Physician>> getPhysicianByName(@PathVariable("name") String name) {
		try {
			List<Physician> physician = new ArrayList<Physician>();

			if (name != null && !name.isEmpty()) {
				phyRepo.findByName(name).forEach(physician::add);
			}

			if (physician.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(physician, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/physician")
	public ResponseEntity<Physician> addPhysician(@RequestBody Physician physic) {
		try {
			Physician physician = phyRepo.save(physic);

			if (null == physician) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(physician, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/availableTimeSlot/{physicianId}")
	public ResponseEntity<Map<Date, List<String>>> availableTimeSlot(@PathVariable("physicianId") Long physicianId) {
		try {
			if (physicianId != null) {
				Optional<Physician> physician = phyRepo.findById(physicianId);
				if (null == physician) {
					return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				} else {
					List<TimeSlot> slots = timeSlotRepo.findByPhysicianId(physicianId);
					if (null != slots && !slots.isEmpty()) {
						Map<Date, List<String>> map = new HashMap<>();
						for (TimeSlot slot : slots) {
							List<String> time = new ArrayList<>();
							if (slot.getSlot1()) {
								LocalTime localtime = physician.get().getStartTime().toLocalTime();
								localtime = localtime.plusMinutes(0 * 30);
								time.add(localtime.toString() + " : " + localtime.plusMinutes(30).toString());
							}
							if (slot.getSlot2()) {
								LocalTime localtime = physician.get().getStartTime().toLocalTime();
								localtime = localtime.plusMinutes(1 * 30);
								time.add(localtime.toString() + " : " + localtime.plusMinutes(30).toString());
							}
							if (slot.getSlot3()) {
								LocalTime localtime = physician.get().getStartTime().toLocalTime();
								localtime = localtime.plusMinutes(2 * 30);
								time.add(localtime.toString() + " : " + localtime.plusMinutes(30).toString());
							}
							if (slot.getSlot4()) {
								LocalTime localtime = physician.get().getStartTime().toLocalTime();
								localtime = localtime.plusMinutes(3 * 30);
								time.add(localtime.toString() + " : " + localtime.plusMinutes(30).toString());
							}
							map.put(slot.getDate(), time);
						}
						return new ResponseEntity<>(map, HttpStatus.OK);
					} else {
						return new ResponseEntity<>(HttpStatus.NO_CONTENT);
					}
				}
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
