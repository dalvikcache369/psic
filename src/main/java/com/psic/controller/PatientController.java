package com.psic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.psic.models.Patient;
import com.psic.repository.PatientRepository;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
public class PatientController {
	@Autowired
	PatientRepository patientRepo;

	@PostMapping("/patient")
	public ResponseEntity<Patient> addPatient(@RequestBody Patient pat) {
		try {
			Patient patient = patientRepo.save(pat);

			if (null == patient) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(patient, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
