package com.psic.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.psic.models.TimeSlot;

@Repository
public interface TimeSlotRepository extends JpaRepository<TimeSlot, Long>{
	
	@Query("select t from TimeSlot t where t.physicianId= ?1 and t.date = ?2")
	TimeSlot getSlotByDate(Long physicianId,Date date);

	@Query("select t from TimeSlot t where t.physicianId= ?1")
	List<TimeSlot> findByPhysicianId(Long physicianId);
}
