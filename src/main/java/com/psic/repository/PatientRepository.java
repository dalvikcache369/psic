package com.psic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.psic.models.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient,Long>{

}
