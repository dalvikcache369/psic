package com.psic.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.psic.models.Physician;

@Repository
public interface PhysicianRepository extends JpaRepository<Physician,Long>{

	@Query("SELECT p FROM Physician p WHERE p.expertises LIKE %?1%")
	List<Physician> findByExpertise(String expertises);

	@Query("SELECT p FROM Physician p WHERE p.name LIKE %?1%")
	List<Physician> findByName(String name);
}
