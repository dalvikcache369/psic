package com.psic.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.psic.models.Treatment;

@Repository
public interface TreatmentRepository extends JpaRepository<Treatment, Long>{

	@Query("select t from Treatment t where t.patientId= ?1 and t.date = ?2")
	List<Treatment> findByPatientIdAndDate(Long patientId, Date date);

}
