package com.psic.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "patient")
public class Patient {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name")
	@NotBlank(message = "name should not be blank")
	@NotNull(message = "name should not be null")
	public String name;
	
	@Column(name = "address")
	public String address;

	@Column(name = "telephone_number")
	@NotBlank(message = "telephone number should not be blank")
	@NotNull(message = "telephone number should not be null")
	@Size(min = 10, message = "telephone number should be 10 digits")
	public String telephoneNumber;

	public Patient(
			@NotBlank(message = "name should not be blank") @NotNull(message = "name should not be null") String name,
			String address,
			@NotBlank(message = "telephone number should not be blank") @NotNull(message = "telephone number should not be null") @Size(min = 10, message = "telephone number should be 10 digits") String telephoneNumber) {
		this.name = name;
		this.address = address;
		this.telephoneNumber = telephoneNumber;
	}

}
