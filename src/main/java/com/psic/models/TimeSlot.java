package com.psic.models;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "timeSlot")
public class TimeSlot {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "physicianId")
	private Long physicianId;
	@Column(name = "date")
	private Date date;
	//each slot will be of half an hour
	@Column(name = "slot1")
	private Boolean slot1;
	@Column(name = "slot2")
	private Boolean slot2;
	@Column(name = "slot3")
	private Boolean slot3;
	@Column(name = "slot4")
	private Boolean slot4;

}
