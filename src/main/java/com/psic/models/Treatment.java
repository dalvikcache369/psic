package com.psic.models;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "treatment")
public class Treatment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "patient_id")
	private Long patientId;
	
	@Column(name = "patient_name")
	private String patientName;
	
	@Column(name = "notes")
	private String visitorName;
	
	@Column(name = "expertise")
	private String expertise;
	
	@Column(name = "room")
	private String room;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "slot")
	private String slot;
	
	@Column(name="physician_id")
	private Long physicianId;
	
	@Column(name="booking_status")
	private String bookingStatus;
	
	@Column(name="availableSlot")
	private Integer availableSlot;

	public Treatment(Long patientId, String patientName, String expertise, String room, Date date, String slot,
			Long physicianId, String bookingStatus,Integer availableSlot) {
		this.patientId = patientId;
		this.patientName = patientName;
		this.expertise = expertise;
		this.room = room;
		this.date = date;
		this.slot = slot;
		this.physicianId = physicianId;
		this.bookingStatus = bookingStatus;
		this.availableSlot=availableSlot;
	}

	public Treatment(String visitorName, String expertise, String room, Date date, String slot, Long physicianId,
			String bookingStatus,Integer availableSlot) {
		this.visitorName = visitorName;
		this.expertise = expertise;
		this.room = room;
		this.date = date;
		this.slot = slot;
		this.physicianId = physicianId;
		this.bookingStatus = bookingStatus;
		this.availableSlot = availableSlot;
	}
	
	
	
}
